#!/bin/bash
#installation de php, apache, mysql...
read -p "version de php (8.1 conseillé):" vphp
apt update 
apt install software-properties-common ca-certificates lsb-release apt-transport-https -y
sh -c 'echo "deb https://packages.sury.org/php/ $(lsb_release -sc) main" > /etc/apt/sources.list.d/php.list'
wget -qO - https://packages.sury.org/php/apt.gpg | sudo apt-key add -
apt update
apt install apache2 libapache2-mod-php$vphp mariadb-server php$vphp  -y
apt install php$vphp-{fpm,common,mysql,sqlite3,zip,gd,mbstring,curl,xml,bcmath} -y
apt install curl openssl unzip curl build-essential libpng-dev gcc make ffmpeg wget unzip -y

cp /etc/php/$vphp/fpm/php.ini /etc/php/$vphp/fpm/php.ini.old
echo "Affectation des valeur dans php.ini..."
echo "memory_limit..."
sed -i 's/memory_limit.*/memory_limit = 1024M/' /etc/php/$vphp/fpm/php.ini
echo "[OK]
upload_max_filesize ..."
sed -i 's/upload_max_filesize.*/upload_max_filesize = 500M/' /etc/php/$vphp/fpm/php.ini
echo "[OK]
post_max_size..."
sed -i 's/post_max_size.*/post_max_size = 500M/' /etc/php/$vphp/fpm/php.ini
echo "[OK]
max_execution_time..."
sed -i 's/max_execution_time.*/max_execution_time = 300/' /etc/php/$vphp/fpm/php.ini
echo "[OK]"


#Creation du fichier koel.conf pour apache
touch /etc/apache2/sites-available/koel.conf

echo "<VirtualHost *:80>" > /etc/apache2/sites-available/koel.conf

read -p "Mail du ServerAdmin (\"Enter\" pour passer): " serverAdmin
if [ $serverAdmin != "" ]
then
	echo "		ServerAdmin $serverAdmin" >> /etc/apache2/sites-available/koel.conf
fi

read  -p "(Sous-)domaine ou serverName (\"Enter\" pour passer): " serverName
if [ $serverName != "" ]
then
	echo "		ServerName $serverName" >> /etc/apache2/sites-available/koel.conf
fi

read -p "ServerAlias (\"Enter\" pour passer): " serverAlias
if [ $serverName != "" ]
then
	echo "		ServerAlias $serverAlias" >> /etc/apache2/sites-available/koel.conf
fi

echo '		DocumentRoot /var/www/html/koel/public
    <FilesMatch \.php$>
		SetHandler "proxy:unix:/run/php/php'$vphp'-fpm-koel.sock|fcgi://localhost"
    </Filesmatch>

    <Directory /var/www/html/koel/public>
        AllowOverride All
        Require all granted
    </Directory>

    Alias /storage /var/www/html/koel/media
    <Directory /var/www/html/koel/media>
        Require all granted
    </Directory>

    LoadModule xsendfile_module modules/mod_xsendfile.so 
    <IfModule mod_xsendfile.c>
    	XSendFile on
 	XSendFilePath /var/www/html/koel/media
    </IfModule>
    ErrorLog ${APACHE_LOG_DIR}/koel-error.log
    CustomLog ${APACHE_LOG_DIR}/koel-access.log combined
RewriteEngine on
RewriteCond %{SERVER_NAME} ='$serverName'
RewriteRule ^ https://%{SERVER_NAME}%{REQUEST_URI} [END,NE,R=permanent]
</VirtualHost>
' >> /etc/apache2/sites-available/koel.conf




#Attribution du nom de l'utilisateur
confirmer="n"
while [ $confirmer != "y" ]
do
	read -p "Nom de l'utilisateur koel (\"Enter\" pour \"koel\"): " user
	if [[ $user == "" ]]
	then
		user=koel
	fi

	read -p "Confirmer l'utilisateur "$user" (y/n)? " confirmer 
done

#Definition du mot de passe 
read -p "Mots de passe de l'utilisateur (/ ! \\ ce mot de passe sera utiliser pour la connection à la BDD et FTP): " mdp
read -p "Comfirmer: " mdp2
while [ "$mdp" != "$mdp2" ]
do
	echo "Saisie incorecte"
	read -p "Mots de passe de l'utilisateur (/ ! \\ ce mot de passe sera utiliser pour la connection à la BDD et FTP): " mdp
	read -p "Comfirmer: " mdp2
done

#Création de l'utilisateur
userPresent=$(cat /etc/group | grep $user | wc -l )
if [[ $userPresent > 0 ]]
then
	echo "L'utilisateur "$user" existe déjà"
else 
	useradd --badnames --password $mdp --home-dir /var/www/html/koel --shell /usr/sbin/nologin $user
fi

#Attribution des droits "www-data":
usermod -aG $user www-data

#Creation du fichier de conf koel.conf pour php
echo "Creation du fichier de configuration koel.conf..."

echo $"; Start a new pool named '"$user"'
; the variable $pool can be used in any directive and will be replaced by the
; pool name ("$user" here)
["$user"]

; Per pool prefix
; It only applies on the following directives:
; - 'access.log'
; - 'slowlog'
; - 'listen' (unixsocket)
; - 'chroot'
; - 'chdir'
; - 'php_values'
; - 'php_admin_values'
; When not set, the global prefix (or /usr) applies instead.
; Note: This directive can also be relative to the global prefix.
; Default Value: none
;prefix = /path/to/pools/$pool

; Unix user/group of processes
; Note: The user is mandatory. If the group is not set, the default user's group
;       will be used.
user = "$user"
group = "$user"

; The address on which to accept FastCGI requests.
; Valid syntaxes are:
;   'ip.add.re.ss:port'    - to listen on a TCP socket to a specific IPv4 address on
;                            a specific port;
;   '[ip:6:addr:ess]:port' - to listen on a TCP socket to a specific IPv6 address on
;                            a specific port;
;   'port'                 - to listen on a TCP socket to all addresses
;                            (IPv6 and IPv4-mapped) on a specific port;
;   '/path/to/unix/socket' - to listen on a unix socket.
; Note: This value is mandatory.
listen = /run/php/php"$vphp"-fpm-koel.sock

; Set listen(2) backlog.
; Default Value: 511 (-1 on FreeBSD and OpenBSD)
;listen.backlog = 511

; Set permissions for unix socket, if one is used. In Linux, read/write
; permissions must be set in order to allow connections from a web server. Many
; BSD-derived systems allow connections regardless of permissions. The owner
; and group can be specified either by name or by their numeric IDs.
; Default Values: user and group are set as the running user
;                 mode is set to 0660
listen.owner = "$user"
listen.group = "$user"
;listen.mode = 0660
; When POSIX Access Control Lists are supported you can set them using
; these options, value is a comma separated list of user/group names.
; When set, listen.owner and listen.group are ignored
;listen.acl_users =
;listen.acl_groups =

; List of addresses (IPv4/IPv6) of FastCGI clients which are allowed to connect.
; Equivalent to the FCGI_WEB_SERVER_ADDRS environment variable in the original
; PHP FCGI (5.2.2+). Makes sense only with a tcp listening socket. Each address
; must be separated by a comma. If this value is left blank, connections will be
; accepted from any ip address.
; Default Value: any
;listen.allowed_clients = 127.0.0.1

; Specify the nice(2) priority to apply to the pool processes (only if set)
; The value can vary from -19 (highest priority) to 20 (lower priority)
; Note: - It will only work if the FPM master process is launched as root
;       - The pool processes will inherit the master process priority
;         unless it specified otherwise
; Default Value: no set
; process.priority = -19

; Set the process dumpable flag (PR_SET_DUMPABLE prctl) even if the process user
; or group is differrent than the master process user. It allows to create process
; core dump and ptrace the process for the pool user.
; Default Value: no
; process.dumpable = yes

; Choose how the process manager will control the number of child processes.
; Possible Values:
;   static  - a fixed number (pm.max_children) of child processes;
;   dynamic - the number of child processes are set dynamically based on the
;             following directives. With this process management, there will be
;             always at least 1 children.
;             pm.max_children      - the maximum number of children that can
;                                    be alive at the same time.
;             pm.start_servers     - the number of children created on startup.
;             pm.min_spare_servers - the minimum number of children in 'idle'
;                                    state (waiting to process). If the number
;                                    of 'idle' processes is less than this
;                                    number then some children will be created.
;             pm.max_spare_servers - the maximum number of children in 'idle'
;                                    state (waiting to process). If the number
;                                    of 'idle' processes is greater than this
;                                    number then some children will be killed.
;  ondemand - no children are created at startup. Children will be forked when
;             new requests will connect. The following parameter are used:
;             pm.max_children           - the maximum number of children that
;                                         can be alive at the same time.
;             pm.process_idle_timeout   - The number of seconds after which
;                                         an idle process will be killed.
; Note: This value is mandatory.
pm = dynamic

; The number of child processes to be created when pm is set to 'static' and the
; maximum number of child processes when pm is set to 'dynamic' or 'ondemand'.
; This value sets the limit on the number of simultaneous requests that will be
; served. Equivalent to the ApacheMaxClients directive with mpm_prefork.
; Equivalent to the PHP_FCGI_CHILDREN environment variable in the original PHP
; CGI. The below defaults are based on a server without much resources. Don't
; forget to tweak pm.* to fit your needs.
; Note: Used when pm is set to 'static', 'dynamic' or 'ondemand'
; Note: This value is mandatory.
pm.max_children = 5

; The number of child processes created on startup.
; Note: Used only when pm is set to 'dynamic'
; Default Value: min_spare_servers + (max_spare_servers - min_spare_servers) / 2
pm.start_servers = 2

; The desired minimum number of idle server processes.
; Note: Used only when pm is set to 'dynamic'
; Note: Mandatory when pm is set to 'dynamic'
pm.min_spare_servers = 1

; The desired maximum number of idle server processes.
; Note: Used only when pm is set to 'dynamic'
; Note: Mandatory when pm is set to 'dynamic'
pm.max_spare_servers = 3

; The number of seconds after which an idle process will be killed.
; Note: Used only when pm is set to 'ondemand'
; Default Value: 10s
;pm.process_idle_timeout = 10s;
; The number of requests each child process should execute before respawning.
; This can be useful to work around memory leaks in 3rd party libraries. For
; endless request processing specify '0'. Equivalent to PHP_FCGI_MAX_REQUESTS.
; Default Value: 0
pm.max_requests = 50

; The URI to view the FPM status page. If this value is not set, no URI will be
; recognized as a status page. It shows the following informations:
;   pool                 - the name of the pool;
;   process manager      - static, dynamic or ondemand;
;   start time           - the date and time FPM has started;
;   start since          - number of seconds since FPM has started;
;   accepted conn        - the number of request accepted by the pool;
;   listen queue         - the number of request in the queue of pending
;                          connections (see backlog in listen(2));
;   max listen queue     - the maximum number of requests in the queue
;                          of pending connections since FPM has started;
;   listen queue len     - the size of the socket queue of pending connections;
;   idle processes       - the number of idle processes;
;   active processes     - the number of active processes;
;   total processes      - the number of idle + active processes;
;   max active processes - the maximum number of active processes since FPM
;                          has started;
;   max children reached - number of times, the process limit has been reached,
;                          when pm tries to start more children (works only for
;                          pm 'dynamic' and 'ondemand');
; Value are updated in real time.
; Example output:
;   pool:                 www
;   process manager:      static
;   start time:           01/Jul/2011:17:53:49 +0200
;   start since:          62636
;   accepted conn:        190460
;   listen queue:         0
;   max listen queue:     1
;   listen queue len:     42
;   idle processes:       4
;   active processes:     11
;   total processes:      15
;   max active processes: 12
;   max children reached: 0
;
; By default the status page output is formatted as text/plain. Passing either
; 'html', 'xml' or 'json' in the query string will return the corresponding
; output syntax. Example:
;   http://www.foo.bar/status
;   http://www.foo.bar/status?json
;   http://www.foo.bar/status?html
;   http://www.foo.bar/status?xml
;
; By default the status page only outputs short status. Passing 'full' in the
; query string will also return status for each pool process.
; Example:
;   http://www.foo.bar/status?full
;   http://www.foo.bar/status?json&full
;   http://www.foo.bar/status?html&full
;   http://www.foo.bar/status?xml&full
; The Full status returns for each process:
;   pid                  - the PID of the process;
;   state                - the state of the process (Idle, Running, ...);
;   start time           - the date and time the process has started;
;   start since          - the number of seconds since the process has started;
;   requests             - the number of requests the process has served;
;   request duration     - the duration in µs of the requests;
;   request method       - the request method (GET, POST, ...);
;   request URI          - the request URI with the query string;
;   content length       - the content length of the request (only with POST);
;   user                 - the user (PHP_AUTH_USER) (or '-' if not set);
;   script               - the main script called (or '-' if not set);
;   last request cpu     - the %cpu the last request consumed
;                          it's always 0 if the process is not in Idle state
;                          because CPU calculation is done when the request
;                          processing has terminated;
;   last request memory  - the max amount of memory the last request consumed
;                          it's always 0 if the process is not in Idle state
;                          because memory calculation is done when the request
;                          processing has terminated;
; If the process is in Idle state, then informations are related to the
; last request the process has served. Otherwise informations are related to
; the current request being served.
; Example output:
;   ************************
;   pid:                  31330
;   state:                Running
;   start time:           01/Jul/2011:17:53:49 +0200
;   start since:          63087
;   requests:             12808
;   request duration:     1250261
;   request method:       GET
;   request URI:          /test_mem.php?N=10000
;   content length:       0
;   user:                 -
;   script:               /home/fat/web/docs/php/test_mem.php
;   last request cpu:     0.00
;   last request memory:  0
;
; Note: There is a real-time FPM status monitoring sample web page available
;       It's available in: /usr/share/php/7.3/fpm/status.html
;
; Note: The value must start with a leading slash (/). The value can be
;       anything, but it may not be a good idea to use the .php extension or it
;       may conflict with a real PHP file.
; Default Value: not set
;pm.status_path = /status

; The ping URI to call the monitoring page of FPM. If this value is not set, no
; URI will be recognized as a ping page. This could be used to test from outside
; that FPM is alive and responding, or to
; - create a graph of FPM availability (rrd or such);
; - remove a server from a group if it is not responding (load balancing);
; - trigger alerts for the operating team (24/7).
; Note: The value must start with a leading slash (/). The value can be
;       anything, but it may not be a good idea to use the .php extension or it
;       may conflict with a real PHP file.
; Default Value: not set
;ping.path = /ping

; This directive may be used to customize the response of a ping request. The
; response is formatted as text/plain with a 200 response code.
; Default Value: pong
;ping.response = pong

; The access log file
; Default: not set
;access.log = log/$pool.access.log

; The access log format.
; The following syntax is allowed
;  %%: the '%' character
;  %C: %CPU used by the request
;      it can accept the following format:
;      - %{user}C for user CPU only
;      - %{system}C for system CPU only
;      - %{total}C  for user + system CPU (default)
;  %d: time taken to serve the request
;      it can accept the following format:
;      - %{seconds}d (default)
;      - %{miliseconds}d
;      - %{mili}d
;      - %{microseconds}d
;      - %{micro}d
;  %e: an environment variable (same as $_ENV or $_SERVER)
;      it must be associated with embraces to specify the name of the env
;      variable. Some exemples:
;      - server specifics like: %{REQUEST_METHOD}e or %{SERVER_PROTOCOL}e
;      - HTTP headers like: %{HTTP_HOST}e or %{HTTP_USER_AGENT}e
;  %f: script filename
;  %l: content-length of the request (for POST request only)
;  %m: request method
;  %M: peak of memory allocated by PHP
;      it can accept the following format:
;      - %{bytes}M (default)
;      - %{kilobytes}M
;      - %{kilo}M
;      - %{megabytes}M
;      - %{mega}M
;  %n: pool name
;  %o: output header
;      it must be associated with embraces to specify the name of the header:
;      - %{Content-Type}o
;      - %{X-Powered-By}o
;      - %{Transfert-Encoding}o
;      - ....
;  %p: PID of the child that serviced the request
;  %P: PID of the parent of the child that serviced the request
;  %q: the query string
;  %Q: the '?' character if query string exists
;  %r: the request URI (without the query string, see %q and %Q)
;  %R: remote IP address
;  %s: status (response code)
;  %t: server time the request was received
;      it can accept a strftime(3) format:
;      %d/%b/%Y:%H:%M:%S %z (default)
;      The strftime(3) format must be encapsuled in a %{<strftime_format>}t tag
;      e.g. for a ISO8601 formatted timestring, use: %{%Y-%m-%dT%H:%M:%S%z}t
;  %T: time the log has been written (the request has finished)
;      it can accept a strftime(3) format:
;      %d/%b/%Y:%H:%M:%S %z (default)
;      The strftime(3) format must be encapsuled in a %{<strftime_format>}t tag
;      e.g. for a ISO8601 formatted timestring, use: %{%Y-%m-%dT%H:%M:%S%z}t
;  %u: remote user
;
; Default: \"%R - %u %t \"%m %r\" %s\"
;access.format = \"%R - %u %t \"%m %r%Q%q\" %s %f %{mili}d %{kilo}M %C%%\"

; The log file for slow requests
; Default Value: not set
; Note: slowlog is mandatory if request_slowlog_timeout is set
;slowlog = log/$pool.log.slow

; The timeout for serving a single request after which a PHP backtrace will be
; dumped to the 'slowlog' file. A value of '0s' means 'off'.
; Available units: s(econds)(default), m(inutes), h(ours), or d(ays)
; Default Value: 0
;request_slowlog_timeout = 0

; Depth of slow log stack trace.
; Default Value: 20
;request_slowlog_trace_depth = 20

; The timeout for serving a single request after which the worker process will
; be killed. This option should be used when the 'max_execution_time' ini option
; does not stop script execution for some reason. A value of '0' means 'off'.
; Available units: s(econds)(default), m(inutes), h(ours), or d(ays)
; Default Value: 0
;request_terminate_timeout = 0

; The timeout set by 'request_terminate_timeout' ini option is not engaged after
; application calls 'fastcgi_finish_request' or when application has finished and
; shutdown functions are being called (registered via register_shutdown_function).
; This option will enable timeout limit to be applied unconditionally
; even in such cases.
; Default Value: no
;request_terminate_timeout_track_finished = no

; Set open file descriptor rlimit.
; Default Value: system defined value
;rlimit_files = 1024

; Set max core size rlimit.
; Possible Values: 'unlimited' or an integer greater or equal to 0
; Default Value: system defined value
;rlimit_core = 0

; Chroot to this directory at the start. This value must be defined as an
; absolute path. When this value is not set, chroot is not used.
; Note: you can prefix with '$prefix' to chroot to the pool prefix or one
; of its subdirectories. If the pool prefix is not set, the global prefix
; will be used instead.
; Note: chrooting is a great security feature and should be used whenever
;       possible. However, all PHP paths will be relative to the chroot
;       (error_log, sessions.save_path, ...).
; Default Value: not set
;chroot =

; Chdir to this directory at the start.
; Note: relative path can be used.
; Default Value: current directory or / when chroot
;chdir = /var/www

; Redirect worker stdout and stderr into main error log. If not set, stdout and
; stderr will be redirected to /dev/null according to FastCGI specs.
; Note: on highloaded environement, this can cause some delay in the page
; process time (several ms).
; Default Value: no
;catch_workers_output = yes

; Decorate worker output with prefix and suffix containing information about
; the child that writes to the log and if stdout or stderr is used as well as
; log level and time. This options is used only if catch_workers_output is yes.
; Settings to \"no\" will output data as written to the stdout or stderr.
; Default value: yes
;decorate_workers_output = no

; Clear environment in FPM workers
; Prevents arbitrary environment variables from reaching FPM worker processes
; by clearing the environment in workers before env vars specified in this
; pool configuration are added.
; Setting to \"no\" will make all environment variables available to PHP code
; via getenv(), $_ENV and $_SERVER.
; Default Value: yes
;clear_env = no

; Limits the extensions of the main script FPM will allow to parse. This can
; prevent configuration mistakes on the web server side. You should only limit
; FPM to .php extensions to prevent malicious users to use other extensions to
; execute php code.
; Note: set an empty value to allow all extensions.
; Default Value: .php
;security.limit_extensions = .php .php3 .php4 .php5 .php7

; Pass environment variables like LD_LIBRARY_PATH. All $VARIABLEs are taken from
; the current environment.
; Default Value: clean env
;env[HOSTNAME] = $HOSTNAME
;env[PATH] = /usr/local/bin:/usr/bin:/bin
;env[TMP] = /tmp
;env[TMPDIR] = /tmp
;env[TEMP] = /tmp

; Additional php.ini defines, specific to this pool of workers. These settings
; overwrite the values previously defined in the php.ini. The directives are the
; same as the PHP SAPI:
;   php_value/php_flag             - you can set classic ini defines which can
;                                    be overwritten from PHP call 'ini_set'.
;   php_admin_value/php_admin_flag - these directives won't be overwritten by
;                                     PHP call 'ini_set'
; For php_*flag, valid values are on, off, 1, 0, true, false, yes or no.

; Defining 'extension' will load the corresponding shared extension from
; extension_dir. Defining 'disable_functions' or 'disable_classes' will not
; overwrite previously defined php.ini values, but will append the new value
; instead.

; Note: path INI options can be relative and will be expanded with the prefix
; (pool, global or /usr)

; Default Value: nothing is defined by default except the values in php.ini and
;                specified at startup with the -d argument
;php_admin_value[sendmail_path] = /usr/sbin/sendmail -t -i -f www@my.domain.com
;php_flag[display_errors] = off
;php_admin_value[error_log] = /var/log/fpm-php.www.log
;php_admin_flag[log_errors] = on
;php_admin_value[memory_limit] = 32M
" > /etc/php/$vphp/fpm/pool.d/koel.conf

#mySQL 
echo "Creation de la base de donnée 'koeldb'..."
mysql -e "CREATE DATABASE koeldb;"
echo "[OK]
Creation de l'utilisateur..."
mysql -e "CREATE USER '${user}'@'localhost' IDENTIFIED BY '${mdp}';"
echo "[OK]
Ajout des privilèges sur koeldb pour l'utilisateur ${user}..."
mysql -e "GRANT ALL PRIVILEGES ON koeldb.* TO '${user}'@'localhost';"
mysql -e "FLUSH PRIVILEGES;"
echo "[OK]"

#cd ~
#curl -sS https://getcomposer.org/installer -o composer-setup.php
#sudo php composer-setup.php --install-dir=/usr/local/bin --filename=composer


wget https://github.com/koel/koel/releases/download/v6.11.1/koel-v6.11.2.zip
unzip koel-v6.11.2.zip
rm koel-v6.11.2.zip
mkdir /var/www/html/koel/
mv koel /var/www/html/koel/app

cp /var/www/html/koel/app/.env.example /var/www/html/koel/app/.env


sed -i 's/DB_HOST=.*/DB_HOST=localhost/' /var/www/html/koel/app/.env
sed -i 's/DB_PORT=.*/DB_PORT=/' /var/www/html/koel/app/.env
sed -i 's/DB_DATABASE=.*/DB_DATABASE=koeldb/' /var/www/html/koel/app/.env
sed -i 's/DB_USERNAME=.*/DB_USERNAME='$user'/' /var/www/html/koel/app/.env
sed -i 's/DB_PASSWORD=.*/DB_PASSWORD='$mdp'/' /var/www/html/koel/app/.env
sed -i 's/STREAMING_METHOD=.*/STREAMING_METHOD=x-sendfile/' /var/www/html/koel/app/.env
sed -i 's/MEMORY_LIMIT=.*/MEMORY_LIMIT=512/' /var/www/html/koel/app/.env
sed -i 's/SPOTIFY_CLIENT_ID=.*/SPOTIFY_CLIENT_ID=07aa93e941944aa98e18894a64586dec/' /var/www/html/koel/app/.env 
sed -i 's/SPOTIFY_CLIENT_SECRET=.*/SPOTIFY_CLIENT_SECRET=bac2dc4d812c442d9408b3e5d0329c27/' /var/www/html/koel/app/.env
sed -i 's/LASTFM_API_KEY=.*/LASTFM_API_KEY=adb640b3f3eb7d6ddfa412b036d1feaf/' /var/www/html/koel/app/.env
sed -i 's/LASTFM_API_SECRET=.*/LASTFM_API_SECRET=a8f787c605580a32bb3b72011f6342e1/' /var/www/html/koel/app/.env
sed -i 's/YOUTUBE_API_KEY=.*/YOUTUBE_API_KEY=AIzaSyAUYNCheH_GwvYXW3-EEQKl97kldl5jA7A/' /var/www/html/koel/app/.env

mkdir /var/www/html/koel/media 
sed -i 's/MEDIA_PATH=.*/MEDIA_PATH=\/var\/www\/html\/koel\/media/' /var/www/html/koel/app/.env

cd /var/www/html/koel/app
php artisan koel:init
php artisan koel:init --no-assets # Populate necessary configurations during the process
#php artisan serve
chown -R $user:$user /var/www/html/koel
chmod -R 775 /var/www/html/koel/app/storage
chmod 775 /var/www/html/koel/app/storage/logs/laravel.log

#Gestion certificat TLS
read -p "Activer le chiffrement TLS ? (y/n)" tls
 
if [ tls == "y" ]; then
	apt install snapd
	snap install core
	snap refresh core
	snap install --classic certbot
	sudo ln -s /snap/bin/certbot /usr/bin/certbot
	certbot -d $serverName
	sed -i 's/APP_URL=.*/APP_URL=https:\/\/'$serverName'/' /var/www/html/koel/app/.env
else
	sed -i 's/APP_URL=.*/APP_URL=http:\/\/'$serverName'/' /var/www/html/koel/app/.env
fi

#Activation de koel
read -p "Voulez-vous desactiver les pages web par défaut ? (y/n)" enable
if [ $enable == "y" ]; then
	a2dissite *default*
fi

a2enconf php$vphp-fpm
a2ensite koel.conf
a2enmod rewrite
systemctl restart apache2.service php$vphp-fpm.service

echo " utilisateur de base :
Username: admin@koel.dev
Password: KoelIsCool"

read -p "Appuyer sur 'Entrer' pour finir la configuration..." finish

#chown -R www-data:www-data /var/www/html/koel/media
#chown -R www-data:www-data /var/www/html/koel/app/storage
systemctl restart apache2.service php$vphp-fpm.service




